
#include "cuerpo.h"
#include <iostream>
#include <math.h>
using namespace std;
cuerpo::cuerpo(double pos_x,double pos_y,double masa,double vel_x,double vel_y,double constg=1)
{   Masa=masa;
    constG=constg;
    Px=pos_x;
    Py=pos_y;
    Vx=vel_x;
    Vy=vel_y;
}
void cuerpo::aceleracion(vector<cuerpo>&objetos,float dt,int evaluar){

        double distancia=0,angulo=0,aceleracionx=0,aceleraciony=0;
        for(int j=0;j<objetos.size();j++){
            if(objetos[evaluar].get_px()!=objetos[j].get_px() && objetos[evaluar].get_py()!=objetos[j].get_py()){
                distancia=sqrt(pow(objetos[j].get_px()-objetos[evaluar].get_px(),2)+pow(objetos[j].get_py()-objetos[evaluar].get_py(),2));
                angulo=atan2((objetos[j].get_py()- objetos[evaluar].get_py()),(objetos[j].get_px()-objetos[evaluar].get_px()));
                aceleracionx+=constG*objetos[j].get_masa()/pow(distancia,2)*cos(angulo);
                aceleraciony+=constG*objetos[j].get_masa()/pow(distancia,2)*sin(angulo);

            }
    }
        Px=Px+Vx*dt+aceleracionx*dt*dt/2;
        Py=Py+Vy*dt+aceleraciony*dt*dt/2;
        Vx=Vx+aceleracionx*dt;
        Vy=Vy+aceleraciony*dt;

}

