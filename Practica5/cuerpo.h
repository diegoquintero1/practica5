#ifndef CUERPO_H
#define CUERPO_H
#include <string>
#include<iostream>
#include <vector>
using namespace std;
class cuerpo
{
private:
    double  Masa;
    double  constG;
    double  Px;
    double  Py;
    double  Vx;
    double  Vy;
public:
    cuerpo(double px,double py,double m,double vx,double vy,double constg);
    void aceleracion(vector<cuerpo>&objetos,float dt,int evaluar);
    double  get_px(){return Px;}
    double  get_py(){return Py;}
    double  get_masa(){return Masa;}
};

#endif//CUERPO_H
